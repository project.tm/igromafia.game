<?php

namespace Igromafia\Game;

class Config {

    const USER_CACHE_TIME = 90;
    const GAME_IBLOCK = 5;
    const SHOP_IBLOCK = 9;
    const ARTICLE_IBLOCK = 4;
    const NEWS_IBLOCK = 1;
    const RELEASE_IBLOCK = 15;
    const REVIEWS_IBLOCK = 11;
    const DZHO_IBLOCK = 14;
    const FAVORIT_HL = 10;

    static public function getIblocName() {
        return array(
            Config::ARTICLE_IBLOCK => array(
                'class' => 'articles',
                'name-header' => 'Статьи',
                'name' => 'Статья'
            ),
            Config::NEWS_IBLOCK => array(
                'class' => 'news',
                'name-header' => 'Новости',
                'name' => 'Новость'
            ),
            Config::RELEASE_IBLOCK => array(
                'class' => 'release',
                'name-header' => 'Релизы',
                'name' => 'Релиз'
            ),
            Config::REVIEWS_IBLOCK => array(
                'class' => 'reviews',
                'name-header' => 'Обзоры',
                'name' => 'Обзор'
            ),
            Config::GAME_IBLOCK => array(
                'class' => 'game',
                'name-header' => 'Игры',
                'name' => 'Игра'
            ),
            Config::DZHO_IBLOCK => array(
                'class' => 'lavkadjoitem',
                'name-header' => 'Лавка джо',
                'name' => 'Лавка джо'
            ),
        );
    }

}
