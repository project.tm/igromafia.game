<?php

namespace Igromafia\Game;

use CFile;

class Image {

    static public function resize($ID, $width, $height) {
        return CFile::ResizeImageGet($ID, array('width' => $width, 'height' => $height), BX_RESIZE_IMAGE_EXACT, true)['src'];
    }

}
