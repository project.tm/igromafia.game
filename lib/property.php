<?php

namespace Igromafia\Game;

use Bitrix\Highloadblock\HighloadBlockTable,
    Project\Core\Utility;

class Property {

    static public function getHl($name) {
        $arData = HighloadBlockTable::getList(array('filter' => array('NAME' => $name)))->fetch();
        return HighloadBlockTable::compileEntity($arData)->getDataClass();
    }

    static public function get($type) {
        static $cache = array();
        if (!isset($cache[$type])) {
            $cache[$type] = Utility::useCache(array(__CLASS__, __FUNCTION__, $type), function() use($type) {
                        $arResult = array();
                        $class = Property::getHl($type);
                        $rsData = $class::getList(array(
                                    "select" => array('UF_XML_ID', 'UF_NAME'),
                        ));
                        $rsData = new \CDBResult($rsData);
                        while ($arItem = $rsData->Fetch()) {
                            $arResult[$arItem['UF_XML_ID']] = $arItem['UF_NAME'];
                        }
                        return $arResult;
                    });
        }
        return $cache[$type];
    }

    static public function getGenres($code) {
        $arResult = self::get('Genres');
        return isset($arResult[$code]) ? $arResult[$code] : '';
    }

    static public function getPlatform($code) {
        $arResult = self::get('Platform');
        return isset($arResult[$code]) ? $arResult[$code] : '';
    }

    static public function getCountry($code) {
        $arResult = self::get('Country');
        return isset($arResult[$code]) ? $arResult[$code] : $code;
    }

}
