<?php

namespace Igromafia\Game;

use Bitrix\Main\Application;

class Sort {

    static public function get($arParams) {
        static $arResult = null;
        $arSort = array("mafia", "user");
        $arSortKey = array(
            "mafia" => 'PROPERTY_RATINGIGROMAFII',
            "user" => 'PROPERTY_RATING'
        );
        $request = Application::getInstance()->getContext()->getRequest();
        $sort = $request->get('sort');
        if(empty($sort)) {
            $sort = defined('TEMPLATES_SET_SORT') ? 'mafia' : '';
        }
        if ($sort and in_array($sort, $arSort)) {
            return array(
                'TYPE' => $sort,
                'KEY' => $arSortKey[$sort]
            );
        }
        return false;
    }

}
