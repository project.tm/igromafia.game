<?php

namespace Igromafia\Game;

class Favorit {

    static private $iblock = array(
        Config::ARTICLE_IBLOCK => 'ARTICLES',
        Config::NEWS_IBLOCK => 'NEWS',
        Config::RELEASE_IBLOCK => 'RELEASE',
        Config::REVIEWS_IBLOCK => 'REVIEWS',
        Config::GAME_IBLOCK => 'GAME',
        Config::DZHO_IBLOCK => 'GAME_DZHO',
    );
    static private $types = array(
        'ARTICLES' => Config::ARTICLE_IBLOCK,
        'NEWS' => Config::NEWS_IBLOCK,
        'RELEASE' => Config::RELEASE_IBLOCK,
        'REVIEWS' => Config::REVIEWS_IBLOCK,
        'GAME' => Config::GAME_IBLOCK,
        'COLLECTION' => Config::GAME_IBLOCK,
        'GAME_DZHO' => Config::DZHO_IBLOCK,
        'LAVKADJOITEM' => Config::DZHO_IBLOCK,
    );

    static public function getIblock($type) {
        return self::$types[$type];
    }

    static public function getType($iblock) {
        return self::$map[$iblock];
    }

}
