<?php

namespace Igromafia\Game\Personal;

use cUser,
    CDBResult,
    Bitrix\Main\Loader,
    Project\Support\Model\ThemeTable;

class Menu {

    static public function get() {
        $userId = cUser::getId();

        $arResult = array();
        if (Loader::includeModule('project.support')) {
            $rsData = ThemeTable::getList(array(
                        'select' => array('ID'),
                        'filter' => array(
                            'USER' => $userId,
                            'ACTIVE' => 1,
                            'BANNED' => 0,
                        ),
                        'limit' => 1
            ));
            $rsData = new CDBResult($rsData);
            if ($arItem = $rsData->Fetch()) {
                $arResult['MESSAGE'] = true;
            }
        }
    }

}
