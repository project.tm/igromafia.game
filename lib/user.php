<?php

namespace Igromafia\Game;

use CUser,
    CFile,
    DateTime,
    CIBlockElement,
    CSaleUserAccount,
    Project\Core\Utility;

class User {

    const DETAIL_URL = "/family/#user_id#/";
    const DETAIL_IMG = '/upload/no-photo.png';
    const INTERVAL_ONLINE = 3600;

    static public function getList($listId) {
        $arResult = array();
        foreach ($listId as $ID) {
            $arResult[$ID] = self::getById($ID);
        }
        return $arResult;
    }

    static public function getById($ID, $isCache = true) {
        if ($isCache) {
            static $arCache = array();
            if (empty($arCache[$ID])) {
                $arCache[$ID] = Utility::useCache(array(__CLASS__, __FUNCTION__, $ID), function() use($ID) {
                            return self::getByIdData($ID);
                        });
            }
            self::isOnline($arCache[$ID]);
            return $arCache[$ID];
        } else {
            return self::getByIdData($ID);
        }
    }

    static private function isOnline(&$arResult) {
         $arResult['IS_ONLINE'] = $arResult["LAST_ACTIVITY_DATE"] ? (time() - self::INTERVAL_ONLINE) < (new DateTime($arResult["LAST_ACTIVITY_DATE"]))->getTimestamp() : false;
    }

    static private function getPublicationText($count) {
        switch ($count) {
            case 0:
                return 'не публикаций';
                break;

            case 1:
                return $count . ' публикация';
                break;

            case 2:
            case 3:
            case 4:
                return $count . ' публикации';
                break;

            default:
                return $count . ' публикаций';
                break;
        }
    }

    static private function getByIdData($ID) {
        $arUser = CUser::GetByID($ID)->Fetch();
        $name = trim($arUser["NAME"]);
        if (!empty($arUser["UF_NICNAME"])) {
            $name = trim($arUser["UF_NICNAME"]);
        }
        if (empty($name)) {
            $name = trim($arUser['LOGIN']);
        }
        $arResult['FULL_NAME'] = $name;
        if (strlen($name) > 15) {
            $name = substr($name, 0, 12) . '...';
        }
        $arResult['ID'] = $ID;
        $arResult['NAME'] = $name;
        $arResult['PERSONAL_CITY'] = $arUser['PERSONAL_CITY'];
        $arResult['PERSONAL_GENDER'] = $arUser['PERSONAL_GENDER'];
        $arResult['LAST_ACTIVITY_DATE'] = $arUser['LAST_ACTIVITY_DATE'];
        self::isOnline($arResult);
        $arResult['PERSONAL_PHOTO'] = $arUser["PERSONAL_PHOTO"] ? Image::resize($arUser["PERSONAL_PHOTO"], 170, 165) : self::DETAIL_IMG;
        $arResult['PERSONAL_PHOTO_BIG'] = $arUser["PERSONAL_PHOTO"] ? Image::resize($arUser["PERSONAL_PHOTO"], 139, 134) : self::DETAIL_IMG;
        $arResult['PERSONAL_PHOTO_MEDIUM'] = $arUser["PERSONAL_PHOTO"] ? Image::resize($arUser["PERSONAL_PHOTO"], 93, 90) : self::DETAIL_IMG;
        $arResult['PERSONAL_PHOTO_SMALL'] = $arUser["PERSONAL_PHOTO"] ? Image::resize($arUser["PERSONAL_PHOTO"], 63, 62) : self::DETAIL_IMG;
        $arResult['DATE_REGISTER'] = FormatDateFromDB($arUser['DATE_REGISTER'], 'SHORT');
        $arResult['BONUS'] = (int) $arUser["UF_POINTS"];
        $arResult['POINTS'] = (int) $arUser["UF_BALL"];
        switch (true) {
            case $arResult['POINTS'] < 500:
                $arResult['STATUS'] = 'ПРОХОЖИЙ';
                break;
            case $arResult['POINTS'] >= 500 and $arResult['POINTS'] < 1000:
                $arResult['STATUS'] = 'СОУЧАСТНИК';
                break;
            case $arResult['POINTS'] >= 1000 and $arResult['POINTS'] < 3000:
                $arResult['STATUS'] = 'СОЛДАТ';
                break;
            case $arResult['POINTS'] >= 3000 and $arResult['POINTS'] < 5000:
                $arResult['STATUS'] = 'КАПОРЕДЖИМЕ';
                break;
            case $arResult['POINTS'] >= 5000:
                $arResult['STATUS'] = 'КОНСИЛЬЕРИ';
                break;
        }

        $arSelect = Array("ID");
        $arFilter = Array("IBLOCK_ID" => [1, 4], "ACTIVE" => "Y", "CREATED_USER_ID" => $ID);
        $res = CIBlockElement::GetList(Array(), $arFilter, false, array('nPageSize' => 1), $arSelect);
        $arResult['URL'] = str_replace("#user_id#", $arUser['ID'], self::DETAIL_URL);
        $arResult['PUBLICATION'] = $res->NavPageCount;

        if ($arResult['PUBLICATION']) {
            $arResult['PUBLICATION_TEXT'] = self::getPublicationText($arResult['PUBLICATION']);
        }
        return $arResult;
    }

    static public function getBonus() {
        $arUser = self::getInfo();
        return $arUser ? $arUser['BONUS'] : 0;
    }

    static public function getInfo($isCache = true) {
        if (!CUser::IsAuthorized()) {
            return false;
        }
        static $arResult = null;
        if (is_null($arResult) or empty($isCache)) {
            $arResult = self::getByIdData(cUser::getId());
        }
        return $arResult;
    }

}
