<?php

namespace Igromafia\Game\Model;

use Bitrix\Main\Entity\DataManager,
    Bitrix\Main;

class FavoritTable extends DataManager {

    public static function getTableName() {
        return 'users_favorits';
    }

    public static function getMap() {
        $fieldsMap = array(
            new Main\Entity\IntegerField('ID', array(
                'primary' => true,
                'autocomplete' => true
                    )),
            new Main\Entity\IntegerField('UF_ID'),
            new Main\Entity\IntegerField('UF_USER'),
            new Main\Entity\StringField('UF_NAME'),
            new Main\Entity\StringField('UF_XML_ID'),
            new Main\Entity\StringField('UF_ENTITY'),
        );
        return $fieldsMap;
    }

}
