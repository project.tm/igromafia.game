<?php

namespace Igromafia\Game;

use CIBlockElement;

class Game {

    const GENRES_SHOOTER = 'QinQsm4I';

    static public function getPlatform($ID) {
        $arResult = array();
        if ($ID) {
            $db_props = CIBlockElement::GetProperty(Config::GAME_IBLOCK, $ID, array('SORT' => "ASC"), array('CODE' => "PLATFORM"));
            while ($ar_props = $db_props->Fetch()) {
                $arResult[] = Property::getPlatform($ar_props['VALUE']);
            }
        }
        return $arResult;
    }

}
