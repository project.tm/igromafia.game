<?php

include (__DIR__ . '/../lib/user.php');

use Bitrix\Main\Application,
    Bitrix\Main\ModuleManager,
    Bitrix\Main\Localization\Loc,
    Bitrix\Main\Loader;

IncludeModuleLangFile(__FILE__);

class igromafia_game extends CModule {

    public $MODULE_ID = 'igromafia.game';

    function __construct() {
        $arModuleVersion = array();

        include(__DIR__ . '/version.php');

        if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }

        $this->MODULE_NAME = Loc::getMessage('IGROMAFIA_PORTAL_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('IGROMAFIA_PORTAL_DESCRIPTION');
        $this->PARTNER_NAME = Loc::getMessage('IGROMAFIA_PORTAL_PARTNER_NAME');
        $this->PARTNER_URI = '';
    }

    public function DoInstall() {
        ModuleManager::registerModule($this->MODULE_ID);
        Loader::includeModule($this->MODULE_ID);
        $this->InstallEvent();
    }

    public function DoUninstall() {
        Loader::includeModule($this->MODULE_ID);
        $this->UnInstallEvent();
        ModuleManager::unRegisterModule($this->MODULE_ID);
    }

    public function InstallEvent() {
        $eventManager = Bitrix\Main\EventManager::getInstance();
        $eventManager->registerEventHandler('iblock', 'OnBeforeIBlockElementAdd', $this->MODULE_ID, '\Igromafia\Game\Event\Iblock', 'OnBeforeIBlockElementAdd');
        $eventManager->registerEventHandler('iblock', 'OnAfterIBlockElementAdd', $this->MODULE_ID, '\Igromafia\Game\Event\Iblock', 'OnAfterIBlockElementAdd');
        $eventManager->registerEventHandler('main', 'OnBeforeProlog', $this->MODULE_ID, '\Igromafia\Game\Event\User', 'OnBeforeProlog');

        $eventManager->registerEventHandler('sale', 'OnSaleDiscountPresetBuildList', $this->MODULE_ID, '\Igromafia\Game\Handlers\DiscountPreset', 'OnAfterAdd');
    }

    public function UnInstallEvent() {
        $eventManager = Bitrix\Main\EventManager::getInstance();
        $eventManager->unRegisterEventHandler('iblock', 'OnBeforeIBlockElementAdd', $this->MODULE_ID, '\Igromafia\Game\Event\Iblock', 'OnBeforeIBlockElementAdd');
        $eventManager->unRegisterEventHandler('iblock', 'OnAfterIBlockElementAdd', $this->MODULE_ID, '\Igromafia\Game\Event\Iblock', 'OnAfterIBlockElementAdd');
        $eventManager->unRegisterEventHandler('main', 'OnBeforeProlog', $this->MODULE_ID, '\Igromafia\Game\Event\User', 'OnBeforeProlog');

        $eventManager->unRegisterEventHandler('sale', 'OnSaleDiscountPresetBuildList', $this->MODULE_ID, '\Igromafia\Game\Handlers\DiscountPreset', 'OnAfterAdd');
    }

}
